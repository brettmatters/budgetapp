import React from 'react';

import TabControl, { Tab } from '../../elements/TabControl';

import CategoryForm from './AddCategory';
import ModifyCategories from './ModifyCategories';

export default function ImportCategorisation() {
    return (
        <TabControl>
            <Tab title="Modify category">
                <ModifyCategories />
            </Tab>
            <Tab title="Add new category">
                <CategoryForm />
            </Tab>
        </TabControl>
    )
}

export function getPrimaryCategory(categories, value) {
    const category = categories.filter((category) => {
        return category.name === value
    })[0];


    return category.subCategory || category.name;
}