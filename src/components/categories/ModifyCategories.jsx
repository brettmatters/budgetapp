import React, { useContext } from 'react';

import { BudgetContext } from '../../contexts/BudgetContext';

export default function ModifyCategories() {
    const { categories, modifyCategory, deleteCategory } = useContext(BudgetContext);

    const handelUpdateName = (index, value) => {
        const category = categories[index];
        modifyCategory(category, {
            name: value
        })
    }

    const handelUpdateSpend = (index, value) => {
        const category = categories[index];
        modifyCategory(category, {
            yearlySpend: parseFloat(value)
        })
    }

    const handelUpdatePrimaryCategory = (index, value) => {
        const category = categories[index];
        modifyCategory(category, {
            subCategory: value
        })
    }

    const handleDeleteCategory = (index) => {
        const category = categories[index];
        deleteCategory(category);
    }

    return (
        <table>
            <thead>
                <tr>
                    <th>Category</th>
                    <th>Parent Category</th>
                    <th>Yearly Spend</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {categories.map((row, index) => {
                    return (
                        <tr key={index}>
                            <td>
                                <input size={100} placeholder="Name" type="text" value={row.name} onChange={(event) => { handelUpdateName(index, event.target.value) }} />
                            </td>
                            <td>
                                {categories.length > 0 && (

                                    <select value={row.subCategory} onChange={(event) => { handelUpdatePrimaryCategory(index, event.target.value) }}>
                                        <option>---</option>
                                        {categories.map((category) => {
                                            return (
                                                <option key={category.name} value={category.name}>{category.displayName}</option>
                                            )
                                        })}
                                    </select>
                                )}
                            </td>
                            <td>
                                <input size={30} placeholder="Spend" type="number" value={row.yearlySpend || 0} onChange={(event) => { handelUpdateSpend(index, event.target.value) }} />
                            </td>
                            <td><button type="button" onClick={() => { handleDeleteCategory(index) }}>Delete</button></td>
                        </tr>
                    )
                })}
            </tbody>

        </table>
    )
}