import React, { useContext, useState } from 'react';

import { BudgetContext } from '../../contexts/BudgetContext';

export default function CategoryForm() {
    const { categories, addCategory } = useContext(BudgetContext);

    const [categoryName, setCategoryName] = useState('');
    const [subCategoryName, setSubCategoryName] = useState();

    const resetForm = () => {
        setCategoryName('')
        setSubCategoryName('');
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        if (categoryName) {
            addCategory({
                name: categoryName,
                subCategory: subCategoryName
            })
        }
        resetForm();
    }

    return (
        <form onSubmit={handleSubmit}>
            <label>
                Category name:
                <input type="text" value={categoryName} onChange={(event) => { setCategoryName(event.target.value) }} />
            </label>
            {categories.length > 0 && (
                <label>
                    Parent category:
                    <select value={subCategoryName} onChange={(event) => { setSubCategoryName(event.target.value) }}>
                        <option>---</option>
                        {categories.map((category) => {
                            return (
                                <option key={category.name} value={category.name}>{category.subCategory && `${category.subCategory} - `}{category.name}</option>
                            )
                        })}
                    </select>
                </label>
            )}

            <input type="submit" value="Submit" />
        </form>
    )
}
