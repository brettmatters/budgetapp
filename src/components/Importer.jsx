import React, { useContext, useState } from 'react';
import Papa from 'papaparse';

import { BudgetContext, Accounts } from '../contexts/BudgetContext';

export default function Importer() {
    const { autoCategories, addTransactions } = useContext(BudgetContext);
    const [account, setAccount] = useState(Accounts.default);
    const [fileUpload, setFileUpload] = useState();
    const [isProcessing, setIsProcessing] = useState(false);

    const resetForm = () => {
        setFileUpload();
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        if (fileUpload) {
            setIsProcessing(true);

            Papa.parse(fileUpload, {
                header: false,
                skipEmptyLines: true,
                error: function (err, file, inputElem, reason) {
                    console.error(err)
                },
                complete: function (results, file) {
                    const transformed = results.data.filter((row) => {
                        // Remove money additions
                        return parseFloat(row[1]) < 0;
                    }).map((row) => {
                        const name = row[2];

                        const autoCategory = autoCategories.find((autoCategory) => {
                            return name.indexOf(autoCategory.matcher) !== -1;
                        })

                        return {
                            id: `${row[0]}, ${row[1]}, ${row[2]}, ${row[3]}`,
                            account: account,
                            date: row[0],
                            amount: parseFloat(row[1]),
                            name,
                            balance: parseFloat(row[3]),
                            category: autoCategory?.category,
                            autoCategorised: !!autoCategory?.category
                        }
                    })

                    addTransactions(transformed);
                    setIsProcessing(false);
                }
            })
        }

        resetForm();
    }

    return (
        <>
            {isProcessing && (<div>Processing File</div>)}

            {!isProcessing && (
                <form onSubmit={handleSubmit} >
                    <label>
                        Import file:
                        <input type="file" accept="text/csv" onChange={(event) => { setFileUpload(event.target.files[0]) }} />
                    </label>

                    <select value={account} onChange={(event) => { setAccount(event.target.value) }}>
                        {Accounts.options.map((account) => {
                            return <option key={account.id} value={account.id}>{account.name}</option>
                        })}
                    </select>

                    <input type="submit" value="Submit" />
                </form>
            )}
        </>

    )
} 