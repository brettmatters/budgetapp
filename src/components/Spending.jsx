import React, { useContext, useState, useEffect } from 'react';
import addMonths from 'date-fns/addMonths'
import isAfter from 'date-fns/isAfter'
import startOfDay from 'date-fns/startOfDay'
import differenceInDays from 'date-fns/differenceInDays'
import { default as parseDate } from 'date-fns/parse';

import { BudgetContext } from '../contexts/BudgetContext';
import { getPrimaryCategory } from './categories/Categories';

const startDate = parseDate('01/06/2021', 'dd/MM/yyyy', new Date());

export default function Spending() {
    const [spending, setSpending] = useState({});
    const [totals, setTotals] = useState([]);
    const [summary, setSummary] = useState({ spend: 0, budget: 0, overs: 0 });
    const { transactions, categories } = useContext(BudgetContext);

    useEffect(() => {
        const newSpending = {};

        transactions.forEach((transaction) => {
            const { category } = transaction;
            const primaryCategory = getPrimaryCategory(categories, category);

            if (category === 'Transfer') {
                return;
            }

            if (!newSpending[category]) {
                const { yearlySpend, displayName } = categories.filter((thisCategory) => {
                    return thisCategory.name === category
                })[0];

                newSpending[category] = {
                    isPrimary: category === primaryCategory,
                    primaryCategory,
                    displayName,
                    budget: yearlySpend,
                    transactions: []
                }
            }

            if (!newSpending[primaryCategory]) {
                const { yearlySpend, displayName } = categories.filter((thisCategory) => {
                    return thisCategory.name === primaryCategory
                })[0];

                newSpending[primaryCategory] = {
                    isPrimary: true,
                    primaryCategory,
                    displayName,
                    budget: yearlySpend,
                    transactions: []
                }
            }

            newSpending[category].transactions.push(transaction);

            if (primaryCategory !== category) {
                newSpending[primaryCategory].transactions.push(transaction);
            }
        })

        setSpending(newSpending);
    }, [transactions, categories])


    useEffect(() => {
        const counters = {};
        let newTotals = [];

        const today = startOfDay(new Date())
        const oneMonth = addMonths(today, -1);
        const oneYear = addMonths(today, -12);

        let summarySpend = 0;
        let summaryBudget = 0;
        let summaryOvers = 0;

        for (const [key, value] of Object.entries(spending)) {
            counters[key] = {
                year: 0,
                month: 0,
                //total: 0
            };

            const { isPrimary, primaryCategory, displayName, budget, transactions } = value;

            transactions.forEach((transaction) => {
                const { amount, date } = transaction;
                const parsedDate = parseDate(date, 'dd/MM/yyyy', new Date());

                //counters[key].total += (amount * 10);

                if (isAfter(parsedDate, oneMonth)) {
                    counters[key].month += (amount * 10);
                }

                if (isAfter(parsedDate, oneYear)) {
                    counters[key].year += (amount * 10);
                }
            });

            const month = Math.round(Math.abs(counters[key].month) / 10);
            const year = Math.round(Math.abs(counters[key].year) / 10);

            let yearlyBudget = Math.round(budget);
            const monthBudget = Math.round(budget / 12);

            // If at least a year hasnt pass this needs to be year to date budget
            if (startDate < oneYear) {
                const daysDiff = differenceInDays(oneYear, startDate)
                const dateFraction = daysDiff / 365;

                yearlyBudget = Math.round(yearlyBudget * dateFraction);
            }

            // Add to summary totals
            if (isPrimary && primaryCategory !== 'N/A') {
                if (year) {
                    summarySpend += year;
                }
                if (yearlyBudget) {
                    summaryBudget += yearlyBudget;
                }

                if (year > yearlyBudget) {
                    summaryOvers += (yearlyBudget - year);
                }
            }

            newTotals.push({
                category: key,
                primaryCategory,
                displayName,
                month,
                year,

                yearlyBudget: yearlyBudget || '',
                monthBudget: monthBudget || '',

                yearlyDifference: year > yearlyBudget ? ` (${yearlyBudget - year})` : '',
                monthDifference: month > monthBudget ? ` (${monthBudget - month})` : '',

                isMonthOverBudget: month > monthBudget,
                isYearOverBudget: year > yearlyBudget,
            })
        }

        newTotals = newTotals.sort((a, b) => (
            a.displayName > b.displayName) ? 1 : ((b.displayName > a.displayName) ? -1 : 0)
        );

        setTotals(newTotals);
        setSummary({ spend: summarySpend, budget: summaryBudget, overs: summaryOvers });
    }, [spending])

    const isOverBudget = { color: 'red' };
    const isRowOverBudget = { background: 'lightgrey' };

    var currency = new Intl.NumberFormat('en-AU', {
        style: 'currency',
        currency: 'AUD',
    });

    return (
        <div>
            <table>
                <thead>
                    <tr>
                        <th>Category</th>
                        <th>Month spend</th>
                        <th>Month budget</th>
                        <th>Year spend</th>
                        <th>Year budget</th>
                    </tr>
                </thead>
                <tbody>
                    {totals.map(({ displayName, category, yearlyBudget, monthBudget, isMonthOverBudget, isYearOverBudget, month, year, yearlyDifference, monthDifference }) => {
                        return (
                            <tr key={category} style={isMonthOverBudget || isYearOverBudget ? isRowOverBudget : {}}>
                                <td>{displayName}</td>
                                <td>{month}</td>
                                <td style={isMonthOverBudget ? isOverBudget : {}}>{monthBudget} {monthDifference}</td>
                                <td>{year}</td>
                                <td style={isYearOverBudget ? isOverBudget : {}}>{yearlyBudget} {yearlyDifference}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>

            <div>
                <h2>Summary</h2>
                <div>Total Spend: {currency.format(summary.spend)}</div>
                <div>Total Budget: {currency.format(summary.budget)}</div>
                <div>Total Over Budget: {currency.format(summary.overs)}</div>
                <div>Expected in account: {currency.format((summary.budget - summary.spend) + Math.abs(summary.overs))}</div>
            </div>
        </div>
    )
}