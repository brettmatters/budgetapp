import React from 'react';

import TransactionsTable from './TransactionsTable';
import TabControl, { Tab } from '../../elements/TabControl';

export default function Transactions() {

    return (
        <TabControl>
            <Tab title="All transactions">
                <TransactionsTable key="all" />
            </Tab>
            <Tab title="Uncategorised transactions">
                <TransactionsTable key="uncategorised" filter="Uncategorised" />
            </Tab>
            <Tab title="Auto categorised transactions">
                <TransactionsTable key="auto" filter="Auto" />
            </Tab>
        </TabControl>

    )
}