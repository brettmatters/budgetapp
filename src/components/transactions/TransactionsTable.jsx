import React, { useState, useContext } from 'react';

import { BudgetContext, Accounts } from '../../contexts/BudgetContext';

export default function TransactionsTable({ filter }) {
    const [categoryFilter, setCategoryFilter] = useState(null);
    const { transactions, modifyTransaction, categories } = useContext(BudgetContext);

    const updateCategory = (id, value) => {
        const index = transactions.findIndex((transaction) => {
            return transaction.id === id;
        })
        const transaction = transactions[index];

        modifyTransaction(transaction, {
            category: value,
            autoCategorised: false
        })
    }

    const filterCategory = (value) => {
        setCategoryFilter(value);
    }

    const accountNames = {};
    Accounts.options.forEach((account) => {
        accountNames[account.id] = account.name;
    })

    return (
        <div>
            <div>
                <select value={categoryFilter} onChange={(event) => { filterCategory(event.target.value); }}>
                    <option value="">---</option>
                    {categories.map((category) => {
                        return (
                            <option key={category.name} value={category.name}>{category.displayName}</option>
                        );
                    })}
                </select>
            </div>
            <table>
                <thead>
                    <tr>
                        <th>Account</th>
                        <th>Date</th>
                        <th>Amount</th>
                        <th>Name</th>
                        <th>Category</th>
                    </tr>
                </thead>
                <tbody>
                    {transactions.filter((transaction) => {
                        if (filter) {
                            if (filter === 'Uncategorised') {
                                return !transaction.category;
                            } else if (filter === 'Auto') {
                                return transaction.autoCategorised;
                            }
                        }
                        return true;
                    }).filter((transaction) => {
                        if (categoryFilter) {
                            if (transaction.category === categoryFilter) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                        return true;
                    }).map((row) => {
                        return (
                            <tr key={row.id}>
                                <td>{accountNames[row.account]}</td>
                                <td>{row.date}</td>
                                <td>{row.amount}</td>
                                <td>{row.name}</td>
                                <td>
                                    <select value={row.category} onChange={(event) => { updateCategory(row.id, event.target.value); }}>
                                        <option value="">---</option>
                                        {categories.map((category) => {
                                            return (
                                                <option key={category.name} value={category.name}>{category.displayName}</option>
                                            );
                                        })}
                                    </select>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>

            </table>
        </div>
    )
}