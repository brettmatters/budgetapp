import React, { useContext, useEffect, useState } from 'react';

import { BudgetContext } from '../../contexts/BudgetContext';

export default function ApplyAutoCategories() {
    const { transactions, modifyTransaction, autoCategories } = useContext(BudgetContext);
    const [categorisedData, setCategorisedData] = useState([]);

    useEffect(() => {
        if (transactions && autoCategories) {
            // Remove items that have a known categorisation
            const uncategorised = transactions.filter((item) => {
                return !item.category;
            })

            setCategorisedData(uncategorised.map((data) => {
                return {
                    ...data,
                    category: '',
                    knownName: '',
                    matcher: ''
                }
            }));
        }

    }, [transactions, autoCategories])

    const findMatcher = (transaction) => {
        const autoCategory = autoCategories.find((autoCategory) => {
            return transaction.name.indexOf(autoCategory.matcher) !== -1;
        })

        return autoCategory?.matcher;
    }

    const findCategory = (transaction) => {
        const autoCategory = autoCategories.find((autoCategory) => {
            return transaction.name.indexOf(autoCategory.matcher) !== -1;
        })

        return autoCategory?.category;
    }

    const applyCategory = (index) => {
        const transaction = categorisedData[index];

        const autoCategory = autoCategories.find((autoCategory) => {
            return transaction.name.indexOf(autoCategory.matcher) !== -1;
        })

        modifyTransaction(transaction, {
            category: autoCategory.category,
            autoCategorised: true
        })
    }

    return (
        <table>
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Amount</th>
                    <th>Name</th>
                    <th>Balance</th>
                    <th>Category</th>
                    <th>Matcher</th>
                    <th>Control</th>
                </tr>
            </thead>
            <tbody>
                {categorisedData.map((row, index) => {
                    return (
                        <tr key={row.id}>
                            <td>{row.date}</td>
                            <td>{row.amount}</td>
                            <td>{row.name}</td>
                            <td>{row.balance}</td>
                            <td>{findCategory(row)}</td>
                            <td>{findMatcher(row)}</td>
                            <td>
                                {findCategory(row) && <button type="button" onClick={() => { applyCategory(index) }}>Apply</button>}

                            </td>
                        </tr>
                    )
                })}
            </tbody>

        </table>
    )
}