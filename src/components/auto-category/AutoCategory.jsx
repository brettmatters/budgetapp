import React from 'react';

import TabControl, { Tab } from '../../elements/TabControl';

import ReviewUnmatched from './ReviewUnmatched';
import ModifyAutoCategories from './ModifyAutoCategories';
import ApplyAutoCategories from './ApplyAutoCategories';

export default function ImportCategorisation() {
    return (
        <TabControl>
            <Tab title="Review un-matched">
                <ReviewUnmatched />
            </Tab>
            <Tab title="Modify auto categories">
                <ModifyAutoCategories />
            </Tab>
            <Tab title="Apply auto categorisation">
                <ApplyAutoCategories />
            </Tab>
        </TabControl>
    )
}