import React, { useContext, useEffect, useState } from 'react';

import { BudgetContext } from '../../contexts/BudgetContext';

export default function ReviewUnmatched() {
    const { transactions, categories, autoCategories, addAutoCategory } = useContext(BudgetContext);
    const [categorisedData, setCategorisedData] = useState([]);

    useEffect(() => {
        if (transactions && autoCategories) {
            // Remove items that have a known categorisation
            const uncategorised = transactions.filter((item) => {
                if (item.category) {
                    return false;
                }
                return !autoCategories.find((autoCategory) => {
                    return item.name.indexOf(autoCategory.matcher) !== -1;
                })
            })

            setCategorisedData(uncategorised.map((data) => {
                return {
                    ...data,
                    category: '',
                    knownName: '',
                    matcher: ''
                }
            }));
        }

    }, [transactions, autoCategories])

    const updateCategory = (index, value) => {
        const newData = [...categorisedData]
        newData[index] = {
            ...categorisedData[index],
            category: value
        };

        setCategorisedData(newData)
    }

    const updateMatchString = (index, value) => {
        const newData = [...categorisedData]
        newData[index] = {
            ...categorisedData[index],
            matcher: value
        };

        setCategorisedData(newData)
    }

    const handleAddKnownItem = (index) => {
        const { category, matcher } = categorisedData[index];

        if (category && matcher) {
            addAutoCategory({
                category, matcher
            })
        }
    }

    return (
        <table>
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Amount</th>
                    <th>Name</th>
                    <th>Balance</th>
                    <th>Add Item</th>
                </tr>
            </thead>
            <tbody>
                {categorisedData.map((row, index) => {
                    return (
                        <tr key={row.id}>
                            <td>{row.date}</td>
                            <td>{row.amount}</td>
                            <td>{row.name}</td>
                            <td>{row.balance}</td>
                            <td>
                                {categories.length > 0 && (

                                    <select value={row.category} onChange={(event) => { updateCategory(index, event.target.value) }}>
                                        <option>---</option>
                                        {categories.map((category) => {
                                            return (
                                                <option key={category.name} value={category.name}>{category.displayName}</option>
                                            )
                                        })}
                                    </select>
                                )}
                                <input placeholder="Matcher" type="text" value={row.matcher} onChange={(event) => { updateMatchString(index, event.target.value) }} />

                                <button type="button" onClick={() => { handleAddKnownItem(index) }}>Add to known items</button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>

        </table>
    )
}