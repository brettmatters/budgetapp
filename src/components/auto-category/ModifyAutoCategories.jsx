import React, { useContext } from 'react';

import { BudgetContext } from '../../contexts/BudgetContext';

export default function ModifyAutoCategories() {
    const { categories, autoCategories, modifyAutoCategory, deleteAutoCategory } = useContext(BudgetContext);

    const handelUpdateMatchString = (index, value) => {
        const autoCategory = autoCategories[index];
        modifyAutoCategory(autoCategory, {
            matcher: value
        })
    }

    const handleUpdateCategory = (index, value) => {
        const autoCategory = autoCategories[index];
        modifyAutoCategory(autoCategory, {
            category: value
        })
    }
    const handleDeleteAutoCategory = (index) => {
        const autoCategory = autoCategories[index];
        deleteAutoCategory(autoCategory);
    }

    return (
        <table>
            <thead>
                <tr>
                    <th>Matcher</th>
                    <th>Category</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {autoCategories.map((row, index) => {
                    return (
                        <tr key={index}>
                            <td>
                                <input size={100} placeholder="Matcher" type="text" value={row.matcher} onChange={(event) => { handelUpdateMatchString(index, event.target.value) }} />
                            </td>
                            <td>
                                {categories.length > 0 && (

                                    <select value={row.category} onChange={(event) => { handleUpdateCategory(index, event.target.value) }}>
                                        <option>---</option>
                                        {categories.map((category) => {
                                            return (
                                                <option key={category.name} value={category.name}>{category.displayName}</option>
                                            )
                                        })}
                                    </select>
                                )}
                            </td>
                            <td><button type="button" onClick={() => { handleDeleteAutoCategory(index) }}>Delete</button></td>
                        </tr>
                    )
                })}
            </tbody>

        </table>
    )
}