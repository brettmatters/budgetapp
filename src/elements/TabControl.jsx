import React, { useState } from 'react';
import PropTypes from 'prop-types';

import './TabControl.scss';

export function Tab({ children }) {
    return <div className="tabContents">{children}</div>
}
Tab.propTypes = {
    title: PropTypes.string
}
Tab.defaultProps = {
    title: "Tab"
}

export default function TabControl({ defaultTabIndex, children }) {
    const [currentTabIndex, setCurrentTabIndex] = useState(defaultTabIndex);

    return (
        <div className="tabControl">
            <ul className="tabControl__list">
                {children.map((child, index) => {
                    return (
                        <li key={index} className="tabControl__item">
                            <button type="button" onClick={() => { setCurrentTabIndex(index) }} className="tabControl__button">
                                {child.props.title}
                            </button>
                        </li>
                    )
                })}
            </ul>
            <div className="tabControl__selectedContents">
                {children.length > 0 ? children[currentTabIndex] : <></>}
            </div>
        </div>
    )
}
TabControl.propTypes = {
    defaultTabIndex: PropTypes.number
}
TabControl.defaultProps = {
    defaultTabIndex: 0
}