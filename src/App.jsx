import React, { useState } from 'react';

import './App.scss';

import BudgetProvider from './contexts/BudgetContext';
import CategoryForm from './components/categories/Categories';
import Importer from './components/Importer';
import Transactions from './components/transactions/Transactions';
import AutoCategory from './components/auto-category/AutoCategory';
import Spending from './components/Spending';

const Views = Object.freeze({
  Categories: Symbol("Categories"),
  Importer: Symbol("Importer"),
  Transactions: Symbol("Transactions"),
  AutoCategory: Symbol("AutoCategory"),
  Spending: Symbol("Spending"),
});


function App() {
  const [currentView, setCurrentView] = useState(Views.Categories)

  return (
    <BudgetProvider>
      <div className="budgetApp">
        <nav className="navbar">
          <ul className="navbar__list">
            <li className="navbar__item">
              <button type="button" onClick={() => { setCurrentView(Views.Categories) }} className="navbar__button">Categories</button>
            </li>
            <li className="navbar__item">
              <button type="button" onClick={() => { setCurrentView(Views.Importer) }} className="navbar__button">Importer</button>
            </li>
            <li className="navbar__item">
              <button type="button" onClick={() => { setCurrentView(Views.Transactions) }} className="navbar__button">Transactions</button>
            </li>
            <li className="navbar__item">
              <button type="button" onClick={() => { setCurrentView(Views.AutoCategory) }} className="navbar__button">AutoCategory</button>
            </li>
            <li className="navbar__item">
              <button type="button" onClick={() => { setCurrentView(Views.Spending) }} className="navbar__button">Spending</button>
            </li>
          </ul>
        </nav>
        <div className='content-area'>
          {currentView === Views.Categories && <CategoryForm />}
          {currentView === Views.Importer && <Importer />}
          {currentView === Views.Transactions && <Transactions />}
          {currentView === Views.AutoCategory && <AutoCategory />}
          {currentView === Views.Spending && <Spending />}
        </div>
      </div>
    </BudgetProvider>
  )
}

export default App;
