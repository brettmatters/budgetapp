import React, { createContext } from 'react';
import PropTypes from 'prop-types';


import useCategories from './useCategories';
import useTransactions from './useTransactions';
import useAutoCategories from './useAutoCategories';

export const BudgetContext = createContext({
    categories: [],
    addCategory: null,
    modifyCategory: null,
    deleteCategory: null,

    transactions: [],
    addTransactions: null,
    modifyTransaction: null,

    autoCategories: [],
    addAutoCategory: null,
    modifyAutoCategory: null,
    deleteAutoCategory: null,
});

export const Accounts = {
    default: '18701784',
    options: [{
        name: 'Bills',
        id: '18701784'
    },
    {
        name: 'Bills Fixed',
        id: '12386730'
    }]
}

BudgetContext.Provider.propTypes = {
    value: PropTypes.shape({
        // Configurations
        categories: PropTypes.arrayOf(
            PropTypes.shape({
                name: PropTypes.string,
                displayName: PropTypes.string,
                subCategory: PropTypes.string,
                yearlySpend: PropTypes.number,
            })
        ),
        addCategory: PropTypes.func,

        // Automatic categorisation settings
        autoCategories: PropTypes.arrayOf(
            PropTypes.shape({
                matcher: PropTypes.string,
                category: PropTypes.string,
            })
        ),
        addAutoCategory: PropTypes.func,

        // Transactions
        transactions: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.string,
                account: PropTypes.string,
                date: PropTypes.string,
                amount: PropTypes.number,
                name: PropTypes.string,
                balance: PropTypes.number,
                category: PropTypes.string,
                autoCategorised: PropTypes.bool,
            })
        ),
        addTransactions: PropTypes.func,

    })
}

export default function BudgetProvider({ children }) {
    const [categories, addCategory, modifyCategory, deleteCategory] = useCategories();
    const [transactions, addTransactions, modifyTransaction, bulkModifyTransactions] = useTransactions();
    const [autoCategories, addAutoCategory, modifyAutoCategory, deleteAutoCategory] = useAutoCategories();

    const handleModifyCategory = (category, changes) => {
        modifyCategory(category, changes);

        // Update existing transactions
        if (changes.name) {
            const categoryTransactions = transactions.filter((transaction) => {
                return transaction.category === category.name;
            })

            bulkModifyTransactions(categoryTransactions, {
                category: changes.name
            });
        }

    }

    return (
        <BudgetContext.Provider value={{
            categories,
            addCategory,
            modifyCategory: handleModifyCategory,
            deleteCategory,

            transactions,
            addTransactions,
            modifyTransaction,

            autoCategories,
            addAutoCategory,
            modifyAutoCategory,
            deleteAutoCategory
        }}>
            {children}
        </BudgetContext.Provider>

    )
}