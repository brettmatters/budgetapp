import { useEffect, useState } from 'react';

function getDisplayName(category) {
    return category.subCategory ? `${category.subCategory} -  ${category.name}` : category.name
}

export default function useCategories() {
    const [categories, setCategories] = useState(function () {
        const categories = JSON.parse(window.localStorage.getItem('budgetCategories')) || [];
        return categories.map((category) => {
            return {
                ...category,
                displayName: getDisplayName(category)
            }
        })
    }());

    const addCategory = (category) => {
        setCategories([...categories].concat({
            ...category,
            displayName: getDisplayName(category)
        }));
    }

    const modifyCategory = (changingCategory, changes) => {
        let allCategories = [...categories];

        const categoryIndex = allCategories.findIndex((category) => {
            return changingCategory.name === category.name;
        })

        const newCategory = {
            ...changingCategory,
            ...changes
        };
        allCategories[categoryIndex] = {
            ...newCategory,
            displayName: getDisplayName(newCategory)
        }

        setCategories(allCategories);
    }

    const deleteCategory = (changingCategory) => {
        let allCategories = [...categories];

        const categoryIndex = allCategories.findIndex((category) => {
            return changingCategory.name === category.name;
        })

        allCategories.splice(categoryIndex, 1);
        setCategories(allCategories);
    }

    useEffect(() => {
        const sortedCategories = categories.sort((a, b) => {
            const aVal = a.subCategory || a.name;
            const bVal = b.subCategory || b.name;
            return aVal === bVal ? 0 : (aVal > bVal ? 1 : -1);
        });

        const cleanedCategories = sortedCategories.map((category) => {
            const cleaned = {
                ...category
            }
            delete cleaned.displayName;
            return cleaned;
        })

        window.localStorage.setItem('budgetCategories', JSON.stringify(cleanedCategories))
    }, [categories]);

    return [categories, addCategory, modifyCategory, deleteCategory];
}