import { useEffect, useState } from 'react';

export default function useAutoCategories() {
    const [autoCategories, setAutoCategories] = useState(function () {
        return JSON.parse(window.localStorage.getItem('budgetAutoCategories')) || []
    }());

    const addAutoCategory = (autoCategory) => {
        setAutoCategories([...autoCategories].concat(autoCategory));
    }

    const modifyAutoCategory = (changingAutoCategory, changes) => {
        let allAutoCategories = [...autoCategories];

        const autoCategoryIndex = allAutoCategories.findIndex((autoCategory) => {
            return changingAutoCategory.matcher === autoCategory.matcher;
        })

        allAutoCategories[autoCategoryIndex] = {
            ...changingAutoCategory,
            ...changes
        }

        setAutoCategories(allAutoCategories);
    }

    const deleteAutoCategory = (changingAutoCategory) => {
        let allAutoCategories = [...autoCategories];

        const autoCategoryIndex = allAutoCategories.findIndex((autoCategory) => {
            return changingAutoCategory.matcher === autoCategory.matcher;
        })

        allAutoCategories.splice(autoCategoryIndex, 1);
        setAutoCategories(allAutoCategories);
    }

    useEffect(() => {
        window.localStorage.setItem('budgetAutoCategories', JSON.stringify(autoCategories))
    }, [autoCategories]);

    return [autoCategories, addAutoCategory, modifyAutoCategory, deleteAutoCategory];
}