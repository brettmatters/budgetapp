import { useEffect, useState } from 'react';
import { default as parseDate } from 'date-fns/parse';

export default function useTransactions() {
    const [transactions, setTransactions] = useState(function () {
        const transactions = JSON.parse(window.localStorage.getItem('budgetTransactions')) || [];
        return transactions.map((transaction) => {
            return {
                ...transaction
                //TODO
            }
        })
    }());

    const addTransactions = (newTransactions) => {
        let allTransactions = [...transactions];

        newTransactions.forEach((newtransaction) => {
            const doesExist = allTransactions.find((transaction) => {
                return newtransaction.id === transaction.id;
            })

            if (!doesExist) {
                console.log(`Adding new transaction ${newtransaction.id}`)
                allTransactions = allTransactions.concat(newtransaction);
            } else {
                //console.log(`Skipping duplicate transaction ${newtransaction.id}`)
            }
        })

        const sortedTransactions = allTransactions.sort(function (a, b) {
            return parseDate(b.date, 'dd/MM/yyyy', new Date()) - parseDate(a.date, 'dd/MM/yyyy', new Date());
        })
        setTransactions(sortedTransactions);
    }

    const modifyTransaction = (changingTransaction, changes) => {
        let allTransactions = [...transactions];

        const transactionIndex = allTransactions.findIndex((transaction) => {
            return changingTransaction.id === transaction.id;
        })

        allTransactions[transactionIndex] = {
            ...changingTransaction,
            ...changes
        }

        const sortedTransactions = allTransactions.sort(function (a, b) {
            return parseDate(b.date, 'dd/MM/yyyy', new Date()) - parseDate(a.date, 'dd/MM/yyyy', new Date());
        })
        setTransactions(sortedTransactions);
    }

    const bulkModifyTransactions = (changingTransactions, changes) => {
        let allTransactions = [...transactions];

        changingTransactions.forEach((changingTransaction) => {
            const transactionIndex = allTransactions.findIndex((transaction) => {
                return changingTransaction.id === transaction.id;
            })

            allTransactions[transactionIndex] = {
                ...changingTransaction,
                ...changes
            }
        })

        const sortedTransactions = allTransactions.sort(function (a, b) {
            return parseDate(b.date, 'dd/MM/yyyy', new Date()) - parseDate(a.date, 'dd/MM/yyyy', new Date());
        })
        setTransactions(sortedTransactions);
    }

    useEffect(() => {
        window.localStorage.setItem('budgetTransactions', JSON.stringify(transactions))
    }, [transactions]);

    return [transactions, addTransactions, modifyTransaction, bulkModifyTransactions];
}